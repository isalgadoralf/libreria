<?php
/* @var $this CompraController */
/* @var $model Compra */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'compra-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'descuento'); ?>
        <?php echo $form->textField($model, 'descuento'); ?>
        <?php echo $form->error($model, 'descuento'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'fecha'); ?>
        
        <?php 
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
             'attribute'=>'fecha',
             'name'=>'fecha',
             'value'=>$model->fecha,
             'model'=>$model,
             'options'=>array(
                 'showAnim'=>'slide',
                 'showButtonPanel'=>true,
                 'dateFormat'=>'yy-m-d',
             ),
            'htmlOptions'=>array('sytle'=>''),
       ));
        
        ?>
       
         <?php echo $form->error($model, 'fecha'); ?>
    </div>

   

    <div class="row">
        <?php echo $form->labelEx($model, 'Proveedor'); ?>

        <?php
       // 
        $models = Proveedor::model()->findAll();

        // format models as $key=>$value with listData
        $list = CHtml::listData($models, 'codigo', 'nombre');
        echo CHtml::dropDownList('Proveedores', $models, $list, array('empty' => '(Select a category)'));
        ?>

        <?php echo $form->error($model, 'Proveedor'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Personal'); ?>
        
        <?php 
         Yii::app()->getModule('personal');
       // echo $form->textField($model, 'Personal_codigo'); 
          //echo $form->textField($model,'Proveedor_codigo'); 
       // $material = new Personal;
        //echo $form->dropDownList($material,'materialesID',
        //CHtml::listData(Materiales::model()->findAll(),'materialesID','Descripcion'),array('prompt'=>'Seleccione')); 
        $models = Personal::model()->findAll();

        // format models as $key=>$value with listData
        $list = CHtml::listData($models, 'codigo', 'nombre');
        echo CHtml::dropDownList('Personal', $models , $list, array('empty' => '(Select a category)'));
        
         
        ?>
        
        <?php echo $form->error($model, 'Personal'); ?>
    </div>
    <div class="row">
        <?php 
        Yii::app()->getModule('inventario');
         $models = Producto::model()->findAll();

        // format models as $key=>$value with listData
        $list = CHtml::listData($models, 'codigo', 'nombre');
        echo CHtml::dropDownList('Productos', $models, $list, array('empty' => '(Select a category)'));
        
        echo CHtml::label('Cantidad','cant');
        echo CHtml::textField('cantidad');
        echo CHtml::checkBox('label_name', array('value' => 1, 'uncheckValue' => 0, 'checked' => 'checked', 'style' => 'margin-top:7px;'));
      
         ?>
    </div>
    
     <div class="row">
        <?php echo $form->labelEx($model, 'total'); ?>
        <?php echo $form->textField($model, 'total'); ?>
        <?php echo $form->error($model, 'total'); ?>
    </div>
     <table>
        <tr>
            <th>id</th>
            <th>Producto</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Sub.Total</th>
        </tr>
        <?php
        if ($valor <> null) {
            //  $va = $valor->getAttribute('Descripcion');
            // $vp = $valor->getAttribute('precio');
//             	echo '<pre>';
//              print_r($valor);
//              echo '</pre>';
//              Yii::app()->end(); 
         //   $max = count($valor);
            $sub  = 0;
            foreach ($valor as $key => $value) {
                $aux = $value->getPrecio()* $value->getCantidad();
                echo "<tr>";
                echo "<td > " . $value->getId() . " </td>";
                echo "<td > " . $value->getDescripcion() . " </td>";
                echo "<td > " . $value->getPrecio() . " </td>";
                echo "<td > " . $value->getCantidad() . " </td>";
                echo "<td > " . $aux . " </td>";

                echo "</tr>";
                $sub = $sub + $aux;
            }
            
        }
    
        ?>
    </table>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
    
     

    <?php $this->endWidget(); ?>

</div><!-- form -->