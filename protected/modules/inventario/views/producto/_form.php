<?php
/* @var $this ProductoController */
/* @var $model Producto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'producto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'precioc'); ?>
		<?php echo $form->textField($model,'precioc'); ?>
		<?php echo $form->error($model,'precioc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'precioV'); ?>
		<?php echo $form->textField($model,'precioV'); ?>
		<?php echo $form->error($model,'precioV'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stock'); ?>
		<?php echo $form->textField($model,'stock'); ?>
		<?php echo $form->error($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stockmin'); ?>
		<?php echo $form->textField($model,'stockmin'); ?>
		<?php echo $form->error($model,'stockmin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stockmax'); ?>
		<?php echo $form->textField($model,'stockmax'); ?>
		<?php echo $form->error($model,'stockmax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Color_codigo'); ?>
		<?php echo $form->textField($model,'Color_codigo'); ?>
		<?php echo $form->error($model,'Color_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Marca_codigo'); ?>
		<?php echo $form->textField($model,'Marca_codigo'); ?>
		<?php echo $form->error($model,'Marca_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Area_codigo'); ?>
		<?php echo $form->textField($model,'Area_codigo'); ?>
		<?php echo $form->error($model,'Area_codigo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Umedida_codigo'); ?>
		<?php echo $form->textField($model,'Umedida_codigo'); ?>
		<?php echo $form->error($model,'Umedida_codigo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->