<?php
/* @var $this UmedidaController */
/* @var $model Umedida */

$this->breadcrumbs=array(
	'Umedidas'=>array('index'),
	$model->codigo=>array('view','id'=>$model->codigo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Umedida', 'url'=>array('index')),
	array('label'=>'Create Umedida', 'url'=>array('create')),
	array('label'=>'View Umedida', 'url'=>array('view', 'id'=>$model->codigo)),
	array('label'=>'Manage Umedida', 'url'=>array('admin')),
);
?>

<h1>Update Umedida <?php echo $model->codigo; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>