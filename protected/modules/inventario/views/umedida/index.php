<?php
/* @var $this UmedidaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Umedidas',
);

$this->menu=array(
	array('label'=>'Create Umedida', 'url'=>array('create')),
	array('label'=>'Manage Umedida', 'url'=>array('admin')),
);
?>

<h1>Umedidas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
