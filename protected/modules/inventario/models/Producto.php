<?php

/**
 * This is the model class for table "producto".
 *
 * The followings are the available columns in table 'producto':
 * @property integer $codigo
 * @property integer $cantidad
 * @property string $nombre
 * @property integer $precioc
 * @property integer $precioV
 * @property integer $stock
 * @property integer $stockmin
 * @property integer $stockmax
 * @property integer $Color_codigo
 * @property integer $Marca_codigo
 * @property integer $Area_codigo
 * @property integer $Umedida_codigo
 */
class Producto extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Color_codigo, Marca_codigo, Area_codigo, Umedida_codigo', 'required'),
			array('cantidad, precioc, precioV, stock, stockmin, stockmax, Color_codigo, Marca_codigo, Area_codigo, Umedida_codigo', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, cantidad, nombre, precioc, precioV, stock, stockmin, stockmax, Color_codigo, Marca_codigo, Area_codigo, Umedida_codigo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigo' => 'Codigo',
			'cantidad' => 'Cantidad',
			'nombre' => 'Nombre',
			'precioc' => 'Precioc',
			'precioV' => 'Precio V',
			'stock' => 'Stock',
			'stockmin' => 'Stockmin',
			'stockmax' => 'Stockmax',
			'Color_codigo' => 'Color Codigo',
			'Marca_codigo' => 'Marca Codigo',
			'Area_codigo' => 'Area Codigo',
			'Umedida_codigo' => 'Umedida Codigo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('precioc',$this->precioc);
		$criteria->compare('precioV',$this->precioV);
		$criteria->compare('stock',$this->stock);
		$criteria->compare('stockmin',$this->stockmin);
		$criteria->compare('stockmax',$this->stockmax);
		$criteria->compare('Color_codigo',$this->Color_codigo);
		$criteria->compare('Marca_codigo',$this->Marca_codigo);
		$criteria->compare('Area_codigo',$this->Area_codigo);
		$criteria->compare('Umedida_codigo',$this->Umedida_codigo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Producto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
