<?php

/**
 * This is the model class for table "venta".
 *
 * The followings are the available columns in table 'venta':
 * @property integer $codigo
 * @property integer $descuento
 * @property string $fecha
 * @property integer $total
 * @property integer $Cliente_codigo
 * @property integer $Personal_codigo
 *
 * The followings are the available model relations:
 * @property Producto[] $productos
 * @property Factura[] $facturas
 * @property Cliente $clienteCodigo
 * @property Personal $personalCodigo
 */
class Venta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'venta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Cliente_codigo, Personal_codigo', 'required'),
			array('descuento, total, Cliente_codigo, Personal_codigo', 'numerical', 'integerOnly'=>true),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, descuento, fecha, total, Cliente_codigo, Personal_codigo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productos' => array(self::MANY_MANY, 'Producto', 'detalleventa(Venta_codigo, Producto_codigo)'),
			'facturas' => array(self::HAS_MANY, 'Factura', 'Venta_codigo'),
			'clienteCodigo' => array(self::BELONGS_TO, 'Cliente', 'Cliente_codigo'),
			'personalCodigo' => array(self::BELONGS_TO, 'Personal', 'Personal_codigo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigo' => 'Codigo',
			'descuento' => 'Descuento',
			'fecha' => 'Fecha',
			'total' => 'Total',
			'Cliente_codigo' => 'Cliente Codigo',
			'Personal_codigo' => 'Personal Codigo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo);
		$criteria->compare('descuento',$this->descuento);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('total',$this->total);
		$criteria->compare('Cliente_codigo',$this->Cliente_codigo);
		$criteria->compare('Personal_codigo',$this->Personal_codigo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Venta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
