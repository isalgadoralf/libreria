<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
   

      public function authenticate()
      {
      $users=array(
      // username => password
      'demo'=>'demo',
      'admin'=>'admin',
      );
      if(!isset($users[$this->username]))
      $this->errorCode=self::ERROR_USERNAME_INVALID;
      elseif($users[$this->username]!==$this->password)
      $this->errorCode=self::ERROR_PASSWORD_INVALID;
      else
      $this->errorCode=self::ERROR_NONE;
      return !$this->errorCode;
      } 

//    private $_id;
//
//    public function authenticate() {
//        Yii::app()->getModule('personal');
//        $username = strtolower($this->username);
//       
//        $user = Personal::model()->find('LOWER(username)=?', array($username));
//        if ($user === null)
//            $this->errorCode = self::ERROR_USERNAME_INVALID;
//        else if (!$user->validatePassword($this->password))
//            $this->errorCode = self::ERROR_PASSWORD_INVALID;
//        else {
//          //  $this->_id = $user->id_usuario;
//         //   $this->username = $user->username;
//          //  $this->errorCode = self::ERROR_NONE;
////            echo '<pre>';
////              print_r(0);
////             // print_r($this);
////              echo '</pre>';
////              Yii::app()->end(); 
//            session_start();
//            $listacompra = array();
//            $_SESSION['listacompra'] = $listacompra;
//            $_SESSION['usuario'] = $user;
//            
//
//            /* Consultamos los datos del usuario por el username ($user->username) */
//         //   $info_usuario = Personal::model()->find('LOWER(username)=?', array($user->username));
//            /* En las vistas tendremos disponibles last_login y perfil pueden setear las que requieran */
//            //  $this->setState('last_login', $info_usuario->last_login);
//            //  $this->setState('perfil', $info_usuario->perfil);
//
//            /* Actualizamos el last_login del usuario que se esta autenticando ($user->username) */
//            /* $sql = "update usuario set last_login = now() where username='$user->username'";
//              $connection = Yii::app()->db;
//              $command = $connection->createCommand($sql);
//              $command->execute(); */
//            $this->errorCode=self::ERROR_NONE;
//        }
//      ///  return $this->errorCode == self::ERROR_NONE;
//        return !$this->errorCode;
//    }
//
//    public function getId() {
//        return $this->_id;
//    }

}
